import React, { Component } from 'react';
import LeadBoard from '../container/leadBoard';
import CourseList from '../container/courseList';
import ApiEndpoint from '../API/endpoints';
import restClient from '../API/restClient';
import '../css/Nurturing.css';
import { Menu, Dropdown, Icon, Slider, Button, Drawer, Divider, Tag, Collapse, Col, Row, Popover } from 'antd';
import Form from '../container/leadForm';
import CollapsePanel from 'antd/lib/collapse/CollapsePanel';

const ButtonGroup = Button.Group;

class Nurturing extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      leadList: [],
      courselist: [],
      stageList: [],
      categoryList: [],
      leadsCount: [],
      selectedCourse: null,
      selected: [],
      selectedUser: null,
      campaignList: [],
      citiesList: [],
      fillMain: {
        contact: {
          gender: null,
          city: null,
          age: [0,100]
        },
        stage: null,
        score: [0,100],
      },
      queriesList: [],

    };

    this.viewLead = this.viewLead.bind(this)
    this.selectMultiforStage = this.selectMultiforStage.bind(this)
    this.runCampaign = this.runCampaign.bind(this);
    this.rmCampaign = this.rmCampaign.bind(this);
    this.sendCampaign = this.sendCampaign.bind(this);
    this.applyFilter = this.applyFilter.bind(this);

  }
  // DB Transactions 
  componentDidMount() {
    let session = JSON.parse(sessionStorage.getItem("session"));
    restClient.request("get", ApiEndpoint.courseList, session ? { sort: "goal", coordinator: session.user._id } : null, { key: 'cources', expireAt: new Date() })
      .then((result) => { this.setState({ courselist: result.data }); },
        (error) => { this.setState({ error }); }
      )

    restClient.request("get", ApiEndpoint.stageList, null, { key: 'stages', expireAt: new Date() })
      .then((result) => { this.setState({ stageList: result.data }); },
        (error) => { this.setState({ error }); }
      )

    restClient.request("get", ApiEndpoint.leadsCount, { action: 'getLeadsAggregateCourse' }, { key: 'leadsCount', expireAt: new Date() })
      .then((result) => { this.setState({ leadsCount: result.data }); },
        (error) => { this.setState({ error }); }
      )

    restClient.request("get", ApiEndpoint.categoryList, null, { key: 'categoryList', expireAt: new Date() })
      .then((result) => { this.setState({ categoryList: result.data }); },
        (error) => { this.setState({ error }); }
      )
    restClient.request("get", ApiEndpoint.campaignList, null, { key: 'campaigns', expireAt: new Date() })
      .then((result) => { this.setState({ campaignList: result.data }); },
        (error) => { this.setState({ error }); }
      )
    restClient.request("get", ApiEndpoint.citiesList, { sort: "name" }, { key: 'citiesList', expireAt: new Date() })
      .then((result) => { this.setState({ citiesList: result.data }); },
        (error) => { this.setState({ error }); }
      )
    restClient.request("get", ApiEndpoint.queriesList, { userId: session.user._id }, { key: 'queriesList', expireAt: new Date() })
      .then((result) => { this.setState({ queriesList: result.data }); },
        (error) => { this.setState({ error }); }
      )
  }
  // Lead Coursewise Filtering
  selectCourse = (selectedCourse) => {
    this.setState({ selectedCourse: selectedCourse });

    restClient.request("get", ApiEndpoint.leadList, { sort: "order", courses: selectedCourse._id }, { key: 'leads', expireAt: new Date() })
      .then((result) => { this.setState({ leadList: result.data }); console.log(result.data) },
        (error) => { this.setState({ error }); }
      )
  }

  // Start User Display and Edit
  viewLead(id) {
    let lda = this.state.leadList.find(item => { return item._id === id })
    this.state.selectedUser = lda

    this.setState({
      showDrawer: true,
      displayUser: true,
    });
  }
  hideLead = () => {
    this.state.selectedUser = null;
    this.setState({
      showDrawer: false
    });
  };

  showEditForm = () => {
    this.setState({
      displayUser: false,
      isNew: false
    });
  }
  showAddForm = () => {
    this.setState({
      showDrawer: true,
      displayUser: false,
      isNew: true
    });
  }
  // End User Display and Edit

  // Start Lead Selection 
  cardSelect = (leadId) => {
    let idx = this.state.selected.findIndex(id => { return id === leadId })
    if (idx === -1)
      this.setState({ selected: [...this.state.selected, leadId] })
    else
      this.setState({ selected: [...this.state.selected.slice(0, idx), ...this.state.selected.slice(idx + 1)] })
  }
  // End Lead Selection 

  // Start Column Selection 
  selectMultiforStage(leads, checky) {
    if (checky) {
      let tempArr = [];
      leads.forEach(le => {
        if (!this.state.selected.includes(le._id)) {
          tempArr.push(le._id);
        }
      });
      this.setState({ selected: [...this.state.selected, ...tempArr] })
    } else {
      let tempi = [];
      tempi = this.state.selected;
      leads.forEach(lead => {
        for (var i = 0; i < tempi.length; i++) {
          if (tempi[i] === lead._id) {
            tempi.splice(i, 1);
          }
        }
      });
      this.setState({ selected: [...tempi] })
    }
  }
  // End Column Selection 

  //Start Filter
  applyFilter(fi) {
    // let sess = JSON.parse(sessionStorage.getItem("session"));
    // console.log(fi);
    // console.log(fi.score[0])
    // console.log(fi.score[1])
    restClient.request("get", ApiEndpoint.leadList,
      {
        sort: "score",
        courses: this.state.selectedCourse._id,
        // score: fi.score[0],
        score: { '$gt': fi.score[0], '$lt': fi.score[1] }
      },

      { key: 'leads', expireAt: new Date() })
      .then((result) => { this.setState({ leadList: result.data }); console.log(result.data) },
        (error) => { this.setState({ error }); }
      )






    // restClient.request("put", ApiEndpoint.queriesList, {
    //   contact: {
    //     gender: fi.contact.gender,
    //     city: fi.contact.city,
    //     age: fi.contact.age,

    //   },
    //   stage: fi.stage,
    //   score: fi.score,
    //   userId: sess.user._id,
    // },
    //   { key: 'queries', expireAt: new Date() })
    //   .then((result) => {
    //     restClient.request("get", ApiEndpoint.queriesList, { userId: sess.user._id }, { key: 'queriesList', expireAt: new Date() })
    //       .then((result) => { this.setState({ queriesList: result.data }); },
    //         (error) => { this.setState({ error }); }
    //       )

    //   },
    //     (error) => { this.setState({ error }); }
    //   )
  }
  //End Filter

  updateItems = (item) => {
    this.setState({ leadList: item });
  }

  // Start Campaign
  runCampaign() {
    let tempArr = [];
    this.state.selected.forEach(snap => {
      tempArr.push(this.state.leadList.find(item => { return item._id === snap }))
    })
    this.setState({
      viewRunCampaign: true,
      campaignLeads: tempArr,
    });
  }
  rmCampaign() {
    this.setState({
      viewRunCampaign: false,
      campaignLeads: null,
      selCampaign: null,
    });
  }
  selectCampaign(camapaign) {
    this.setState({ selCampaign: camapaign });
  }
  sendCampaign() {
    console.log(this.state.selCampaign);
    console.log(this.state.campaignLeads);
  }

  // End Campaign


  onSave = (result) => {
    this.hideLead();
    this.selectCourse(this.state.selectedCourse);

  }

  render() {
    const { error, stageList, leadList, campaignList, queriesList, fillMain, selCampaign, citiesList, campaignLeads, displayUser, selected, courselist, selectedUser, selectedCourse, leadsCount, categoryList } = this.state;
    const marks = {
      0: 0,
      100: 100
    }
    const courseMenu = (
      <Menu onClick={(menu) => {
        let selected = courselist.filter((course) => { return course._id === menu.key });
        this.selectCourse(selected[0]);
      }} >
        {
          courselist.map((course) => {
            return <Menu.Item key={course._id} >
              {course.name}
            </Menu.Item>
          })
        }
      </Menu>
    );
    const genExtra = (lead) => (
      <Icon
        type="delete"
        onClick={(event) => {
          let temp = campaignLeads;
          for (var i = 0; i < temp.length; i++) {
            if (temp[i]._id === lead._id) {
              temp.splice(i, 1);
            }
          }
          this.setState({
            campaignLeads: temp
          })

          event.stopPropagation();
        }}
      />
    );
    const campaignMenu = (
      <Menu onClick={(menu) => {
        let selCampaign = campaignList.filter((camapign) => { return camapign.campaignId === menu.key });
        this.selectCampaign(selCampaign[0]);
      }} >
        {
          campaignList.map((campaign) => {
            return <Menu.Item key={campaign.campaignId} >
              {campaign.name}
            </Menu.Item>
          })
        }
      </Menu>
    );
    const citiesDrop = (
      <Menu onClick={(menu) => {
        let temp = this.state.fillMain;
        temp.contact.city = menu.key;
        this.setState({ fillMain: temp })
      }} >
        {
          citiesList.map((city) => {
            return <Menu.Item key={city.name} >
              {city.name}
            </Menu.Item>
          })
        }
      </Menu>
    );
    let gendersList = [
      {
        id: "male",
        name: "male"
      },
      {
        id: "female",
        name: "female"
      },
      {
        id: "other",
        name: "other"
      }
    ]
    const genderDrop = (
      <Menu onClick={(menu) => {
        let temp = this.state.fillMain;
        temp.contact.gender = menu.key;
        this.setState({ fillMain: temp })
      }} >
        {
          gendersList.map((gender) => {
            return <Menu.Item key={gender.name} >
              {gender.name}
            </Menu.Item>
          })
        }
      </Menu>
    );


    const stagesDrop = (
      <Menu onClick={(menu) => {
        let temp = this.state.fillMain;
        temp.stage = menu.key;
        this.setState({ fillMain: temp })
      }} >
        {
          stageList.map((stages) => {
            return <Menu.Item key={stages.name} >
              {stages.name}
            </Menu.Item>
          })
        }
      </Menu>
    );

    let isMultiselected = this.state.selected.length > 0 ? true : false;
    let isSingleSelected = this.state.selected.length === 1 ? true : false;
    return (
      <div>
        <h3>Nurturing</h3>
        {/* User Drawer   */}
        <Drawer
          title="Add new candidate"
          width={720}
          onClose={this.hideLead}
          visible={this.state.showDrawer}
        >
          {displayUser ?
            <div>
              <Button onClick={this.showEditForm} className='command rght' > <Icon type="edit" /> Edit </Button>
              {selectedUser ?
                <div>
                  <p>Id : {selectedUser.leadId}</p>

                  <p>Courses</p>
                  {selectedUser.courses.map((c, i) => {
                    return <li key={c._id}>{c._id}</li>
                  })}
                  <p>Stage :{selectedUser.stage.name}</p>


                  <p>Score : {selectedUser.score}</p>
                  <p>Tags{selectedUser.tags.map((s, i) => {
                    return <Tag key={s} >{s}</Tag>
                  })}
                  </p>


                  <p>Notes</p>
                  {selectedUser.notes.map((n, i) => {
                    return <li key={n}>{n}</li>
                  })}


                  <Divider>Personal info</Divider>

                  <p>Title : {selectedUser.contact.title}</p>
                  <p>First Name : {selectedUser.contact.firstName}</p>
                  <p>Last Name : {selectedUser.contact.lastName}</p>
                  <p>Gender : {selectedUser.contact.gender}</p>
                  <p>DOB : {selectedUser.contact.dob}</p>

                  <Divider>Contact info</Divider>

                  <p>Mobile : {selectedUser.contact.mobile}</p>
                  <p>E-mail : {selectedUser.contact.email}</p>

                  <Divider>Address</Divider>

                  <p>Country : {selectedUser.contact.country}</p>

                  <p>City : {selectedUser.contact.city}</p>

                  <p>Zipcode : {selectedUser.contact.zipCode}</p>

                  <p>Address line 1 : {selectedUser.contact.addressLine1}</p>

                  <p>Address line 2 : {selectedUser.contact.addressLine2}</p>


                </div>
                : null
              }
            </div> :
            <Form
              leadData=
              {!this.state.isNew ? selectedUser : null}
              courselist={courselist} stageList={stageList} categoryList={categoryList} selectedCourse={selectedCourse} onSave={this.onSave}   >
            </Form>
          }

        </Drawer>
        {/* Campaign Drawer   */}
        <Drawer

          title="Run Campaign"
          width={720}
          onClose={this.rmCampaign}
          visible={this.state.viewRunCampaign}
        >

          <Dropdown overlay={campaignMenu}>
            <div className="courseSelector" >    {selCampaign ? selCampaign.name : "Select Campaign"}      <Icon type="down" /> </div>
          </Dropdown>

          <Divider>Campaign Leads</Divider>
          {campaignLeads ?

            campaignLeads.map(lead => {
              return (
                <Collapse key={lead._id}>
                  <CollapsePanel header={lead.contact.firstName} key={lead.contact.firstName} extra={genExtra(lead)}>
                    <p>Score : {lead.score}</p>
                    <p>Stage : {lead.stage.name}</p>
                  </CollapsePanel>

                </Collapse>
              )
            })
            : null}

          {selCampaign ?
            <Button onClick={this.sendCampaign} className='command rght mr-top' >
              Send Campaign
          </Button> : null}

        </Drawer>


        <Dropdown overlay={courseMenu}>
          <div className="courseSelector" >   {selectedCourse ? selectedCourse.name : "select course"}      <Icon type="down" /> </div>
        </Dropdown>
        {selectedCourse ? <div><ButtonGroup className='commandbar' >
          <Button onClick={this.showAddForm} className='command' ><Icon type="plus" /> Add    </Button>
          <Button className='command' > <Icon type="export" /> Export </Button>
          {/* {isSingleSelected ? <Button onClick={this.showEditForm} className='command' > <Icon type="edit" /> Edit </Button> : null} */}
          {isMultiselected ? <Button className='command' > <Icon type="delete" /> Delete </Button> : null}
          {isMultiselected ? <Button onClick={this.runCampaign} className='command' ><Icon type="solution" /> Run campaign </Button> : null}

        </ButtonGroup>

          {/* {isMultiselected ? <Button className='command' onClick={() => { this.selected = null; }} > <Icon type="export" /> Deselect All </Button> : null} */}


          <div className="filtersDiv" >
            <Row>

              <Col md={4}>
                <Dropdown overlay={citiesDrop}>
                  <div className="fil" >  {fillMain.contact.city ? fillMain.contact.city : "Select a city"}       <Icon type="down" /> </div>
                </Dropdown>
              </Col>

              <Col md={4}>
                <Dropdown overlay={genderDrop}>
                  <div className="fil" >     {fillMain.contact.gender ? fillMain.contact.gender : "Select a Gender"}      <Icon type="down" /> </div>
                </Dropdown>
              </Col>

              <Col md={4}>
                <Dropdown overlay={stagesDrop}>
                  <div className="fil" >   {fillMain.stage ? fillMain.stage : "Select a Stage"}      <Icon type="down" /> </div>
                </Dropdown>
              </Col>

            </Row>

            <br />

            <Row>


              <Col md={10}>
                < p > Score</p>
                <Slider marks={marks} onChange={(sc) => { let temp = this.state.fillMain; temp.score = sc; this.setState({ fillMain: temp }); }} range defaultValue={[0, 100]} />
              </Col>

              <Col md={{ span: 10, offset: 2 }}>
                < p > Age</p>
                <Slider marks={marks} onChange={(vali) => { let temp = this.state.fillMain; temp.contact.age = vali; this.setState({ fillMain: temp }) }} range defaultValue={[0, 100]} />
              </Col>


            </Row>
          </div>
          <Button className='applyBtn' onClick={() => { this.applyFilter(fillMain) }} > Apply Filter </Button>

          <Row>
            {queriesList ?
              <p>Recent Filters : </p>
              : null}
            {queriesList ?
              queriesList.map(snap => {
                return (
                  <Popover key={snap.score}
                    content={
                      <div>
                        {snap.stage ? <p>Stage : {snap.stage}</p> : null}
                        {snap.score ? <p>Score : {snap.score}</p> : null}
                        {snap.contact.age ? <p>Age : {snap.contact.age}</p> : null}
                        {snap.contact.city ? <p>City : {snap.contact.city}</p> : null}
                        {snap.contact.gender ? <p>Gender : {snap.contact.gender}</p> : null}
                      </div>
                    }
                    title="Filter" >
                    {snap.contact.city ? <Tag>{snap.contact.city}</Tag> : <Tag>No City</Tag>}
                  </Popover>
                )
              })
              : null}

          </Row>



          <LeadBoard stages={stageList} items={leadList} updateItems={this.updateItems} selectMultiforStage={this.selectMultiforStage} viewLead={this.viewLead} cardSelect={this.cardSelect} selected={selected} ></LeadBoard></div> : <CourseList select={this.selectCourse} leadsCount={leadsCount} categoryList={categoryList} items={courselist} ></CourseList>}
      </div>
    );
  }
}

export default Nurturing;
