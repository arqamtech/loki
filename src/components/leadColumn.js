import React, { Component } from 'react';
import LeadListItem from './leadListItem';
import { List, Checkbox } from 'antd';

class LeadColumn extends Component {

  render() {
    const { items, stage, cardSelect, cardDroped, selectMultiforStage, viewLead, selected } = this.props;

    return (
      <List
        size="large"
        header={
          <div className="stageDiv" >
            <Checkbox
              onChange={(e) => { selectMultiforStage(items, e.target.checked) }}
              className="stageChek"></Checkbox>
            <h4>{stage.name}</h4>
          </ div>
        }
        bordered={false}
        dataSource={items}
        renderItem={item => (<List.Item>  <LeadListItem selected={selected} selectMultiforStage={selectMultiforStage} viewLead={viewLead} cardSelect={cardSelect} cardDroped={cardDroped} item={item} ></LeadListItem> </List.Item>)}
      >
      </List>
    );
  }
}
export default LeadColumn;
